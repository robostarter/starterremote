# StarterRemote

Universal Arduino library for IR start modules programming, starting and stopping.

The protocol it implements can be found in details here: https://p1r.se/startmodule/implement-yourself/

The library should work on any Arduino board (incl. compatible boards).