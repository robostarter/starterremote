// Example
// Here you can see how to use the library for the starter module.
// NOTE: Do not forget to set the pin to OUTPUT as the LED will require power
// Schematic:
// Connect an IR led to pin 9 and GND (longer leg goes to pin 9, shorter to GND)

#include <StarterRemote.h>

// Global object for the StarterRemote itself, initialized with the PIN that will drive the LED
StarterRemote myStarter(9);

void setup()
{
  // We need to set the pin to output as it will drive the LED
  pinMode(9, OUTPUT);
}

void loop()
{
  // First we send a programming command to the module so it understands our future commands
  // you might want to do this with buttons for a remote implementation
  // The given parameter is the address to which it will program the module receiving the command
  myStarter.sendReprogramCommand(10);
  // We wait 5 sec so we can observe the LED on the module flashing
  delay( 5000 );
  // Start command again receives the address to which we just programmed the module to respond (the Dohyo address)
  myStarter.sendStartCommand(10);
  // The module LED should be constantly on, until we send the stop command
  delay( 4000 );
  // The stop command is again needing the address of the Dohyo
  myStarter.sendStopCommand(10);
  // We wait 20 sec before doing all again
  delay( 20000 );
}
